**PER SITE COLLECTION**

These steps will install and activate the Tool for one single site collection. Repeat steps 3 onward for each site collection where the Tool is to be installed.

1.	Download the latest version of the Tool from your BindTuning account;

	![Picture1.png](https://bitbucket.org/repo/BnA98G/images/2584845173-Picture1.png)

2.	Open **Site Settings** at the root of your site collection;

	![Picture2.png](https://bitbucket.org/repo/BnA98G/images/1518995613-Picture2.png)

3.	Select Solutions under **Web Designer Galleries**;

4.	Click **Upload Solution**, on the top ribbon;

5.	Upload the .wsp file from the Tool’s package;

6.	Click OK and wait for the dialog to finish loading;

7.	Click Activate.

Congratulations! The Tool is now installed and activated for this site collection!
