1.	Locate the Collapsible Ribbon control in the page;
2.	Click the Collapsible Ribbon control to toggle the ribbon visibility.


####Desktop

![Picture6.png](https://bitbucket.org/repo/BnA98G/images/2176973617-Picture6.png)


####Mobile

![Picture7.png](https://bitbucket.org/repo/BnA98G/images/1976192595-Picture7.png)
